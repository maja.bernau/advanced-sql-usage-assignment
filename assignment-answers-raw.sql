-- 1. Views:
-- 1.1 Solution: 
CREATE VIEW warehouse_shipments AS
SELECT s.tracking_number, s.weight, s.status, w.name AS warehouse_name
FROM Shipments s
INNER JOIN warehouses w ON w.warehouse_id = s.warehouse_id;

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 1.2 Solution: 
CREATE VIEW carrier_shipments AS
SELECT s.tracking_number, s.weight, s.status, ca.name AS carrier_name
FROM Shipments s
INNER JOIN carriers ca ON ca.carrier_id  = s.carrier_id;

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 2. Common Table Expressions (CTEs):
-- 2.1 Solution: 
WITH pending_shipments (tracking_number, weight, location) AS (
SELECT s.tracking_number, s.weight, w.location
FROM shipments s
INNER JOIN warehouses w ON w.warehouse_id = s.warehouse_id
WHERE s.status = 'Pending'
)
SELECT *
FROM pending_shipments;

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 2.2 Solution: 
WITH heavy_shipments (tracking_number, weight, carrier_name) AS (
SELECT s.tracking_number, s.weight, ca.name
FROM shipments s
INNER JOIN carriers ca ON ca.carrier_id = s.carrier_id
WHERE s.weight > 200
)
SELECT *
FROM heavy_shipments;

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 3. Transactions:
-- 3.1 Solution: 
BEGIN;
UPDATE Shipments 
SET status = 'Shipped',
	weight = weight + 10
WHERE tracking_number = 'ABC123';
COMMIT; 

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 3.2 Solution: 
BEGIN;
INSERT INTO Shipments (tracking_number, weight, warehouse_id, carrier_id)
VALUES ('LMN456', 180.75, 2,2);
COMMIT; 

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- 4. Indexes: 
-- Solution: 
CREATE INDEX inx_tracking_number ON shipments (tracking_number);

