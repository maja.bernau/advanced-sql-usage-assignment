## Advanced SQL usage assignment

- Assignment done in Jupyter Notebook, exported and saved as README.md
- SQL code answers are also copied to `assignment-answers-raw.sql`, if that's a more convenient format

### Preparation before doing tasks

##### Create a database using `psql`
* Connect using: `psql -U postgres`
* Create DB: `CREATE DATABASE advanced_sql_usage_assignment;
`

##### Load SQL functionality into the notebook and connect to a PostgreSQL database


```python
%load_ext sql
%sql postgresql://postgres:password@localhost:5432/advanced_sql_usage_assignment
```

##### Create tables according to instructions (but with some more dummy data)


```sql
%%sql

DROP VIEW IF EXISTS warehouse_shipments;
DROP VIEW IF EXISTS carrier_shipments;
DROP TABLE IF EXISTS shipments;
DROP TABLE IF EXISTS warehouses;
DROP TABLE IF EXISTS carriers;

CREATE TABLE warehouses (
    warehouse_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    location VARCHAR(100) NOT NULL
);

CREATE TABLE carriers (
    carrier_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    contact_person VARCHAR(50),
    contact_number VARCHAR(15)
);

CREATE TABLE shipments (
    shipment_id SERIAL PRIMARY KEY,
    tracking_number VARCHAR(20) UNIQUE NOT NULL,
    weight DECIMAL(10, 2) NOT NULL,
    status VARCHAR(20) DEFAULT 'Pending',
    warehouse_id INT REFERENCES warehouses(warehouse_id),
    carrier_id INT REFERENCES carriers(carrier_id)
);

INSERT INTO warehouses (name, location) VALUES
    ('Warehouse A', 'Stockholm'),
    ('Warehouse B', 'Helsinki'),
    ('Warehouse C', 'Oslo'),
    ('Warehouse D', 'Copenhagen'),
    ('Warehouse E', 'Berlin'),
    ('Warehouse F', 'Paris'),
    ('Warehouse G', 'London'),
    ('Warehouse H', 'Rome'),
    ('Warehouse I', 'Madrid'),
    ('Warehouse J', 'Athens');

INSERT INTO carriers (name, contact_person, contact_number) VALUES
    ('Carrier X', 'John Doe', '123-456-7890'),
    ('Carrier Y', 'Jane Smith', '987-654-3210'),
    ('Carrier Z', 'Alex Adams', '111-222-3333'),
    ('Carrier W', 'Chris Carter', '222-333-4444'),
    ('Carrier V', 'Pat Powell', '333-444-5555'),
    ('Carrier U', 'Taylor Thompson', '444-555-6666'),
    ('Carrier T', 'Sam Sanders', '555-666-7777'),
    ('Carrier S', 'Jamie Jones', '666-777-8888'),
    ('Carrier R', 'Morgan Miller', '777-888-9999'),
    ('Carrier Q', 'Jordan Jackson', '888-999-0000');

DO $$
BEGIN
    FOR i IN 1..100000 LOOP
        INSERT INTO shipments (tracking_number, weight, warehouse_id, carrier_id)
        VALUES (
            'ABC' || i::TEXT,               -- tracking_number as 'ABC' and increment using loop counter to ensure uniqueness 
            (RANDOM() * 205),               -- weight from 0 to 205
            (RANDOM() * 9)::INT + 1,        -- plus with 1 since RANDOM() can return 0
            (RANDOM() * 9)::INT + 1         -- plus with 1 since RANDOM() can return 0
        );
    END LOOP;
END$$;
```

     * postgresql://postgres:***@localhost:5432/advanced_sql_usage_assignment
    Done.
    Done.
    Done.
    Done.
    Done.
    Done.
    Done.
    Done.
    10 rows affected.
    10 rows affected.
    Done.
    




    []



### Tasks

#### 1.Views

1.1. Create a view named `warehouse_shipments` that displays the tracking number, weight, and status of shipments along with the warehouse name for each shipment.

###### Solution: 


```sql
%%sql

CREATE VIEW warehouse_shipments AS
SELECT s.tracking_number, s.weight, s.status, w.name AS warehouse_name
FROM Shipments s
INNER JOIN warehouses w ON w.warehouse_id = s.warehouse_id;

-- test view and limit result to not bloat notebook
SELECT * 
FROM warehouse_shipments
LIMIT 5;
```

     * postgresql://postgres:***@localhost:5432/advanced_sql_usage_assignment
    Done.
    5 rows affected.
    




<table>
    <thead>
        <tr>
            <th>tracking_number</th>
            <th>weight</th>
            <th>status</th>
            <th>warehouse_name</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>ABC1</td>
            <td>117.57</td>
            <td>Pending</td>
            <td>Warehouse I</td>
        </tr>
        <tr>
            <td>ABC2</td>
            <td>1.10</td>
            <td>Pending</td>
            <td>Warehouse J</td>
        </tr>
        <tr>
            <td>ABC3</td>
            <td>202.36</td>
            <td>Pending</td>
            <td>Warehouse D</td>
        </tr>
        <tr>
            <td>ABC4</td>
            <td>22.73</td>
            <td>Pending</td>
            <td>Warehouse E</td>
        </tr>
        <tr>
            <td>ABC5</td>
            <td>125.00</td>
            <td>Pending</td>
            <td>Warehouse B</td>
        </tr>
    </tbody>
</table>



1.2. Create another view named `carrier_shipments` that shows the tracking number, weight, and status of shipments along with the carrier name for each shipment.

###### Solution: 


```sql
%%sql

CREATE VIEW carrier_shipments AS
SELECT s.tracking_number, s.weight, s.status, ca.name AS carrier_name
FROM Shipments s
INNER JOIN carriers ca ON ca.carrier_id  = s.carrier_id;

-- test view and limit result to not bloat notebook
SELECT * 
FROM carrier_shipments
LIMIT 5; 
```

     * postgresql://postgres:***@localhost:5432/advanced_sql_usage_assignment
    Done.
    5 rows affected.
    




<table>
    <thead>
        <tr>
            <th>tracking_number</th>
            <th>weight</th>
            <th>status</th>
            <th>carrier_name</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>ABC1</td>
            <td>117.57</td>
            <td>Pending</td>
            <td>Carrier S</td>
        </tr>
        <tr>
            <td>ABC2</td>
            <td>1.10</td>
            <td>Pending</td>
            <td>Carrier V</td>
        </tr>
        <tr>
            <td>ABC3</td>
            <td>202.36</td>
            <td>Pending</td>
            <td>Carrier W</td>
        </tr>
        <tr>
            <td>ABC4</td>
            <td>22.73</td>
            <td>Pending</td>
            <td>Carrier Y</td>
        </tr>
        <tr>
            <td>ABC5</td>
            <td>125.00</td>
            <td>Pending</td>
            <td>Carrier W</td>
        </tr>
    </tbody>
</table>



#### 2. Common Table Expressions (CTEs)

2.1. Create a CTE named `pending_shipments` that includes the tracking number, weight, and warehouse location for all shipments with the status 'Pending'.

###### Solution: 


```sql
%%sql

WITH pending_shipments (tracking_number, weight, location) AS (
SELECT s.tracking_number, s.weight, w.location
FROM shipments s
INNER JOIN warehouses w ON w.warehouse_id = s.warehouse_id
WHERE s.status = 'Pending'
)
SELECT *
FROM pending_shipments
LIMIT 5; -- limit result to not bloat notebook
```

     * postgresql://postgres:***@localhost:5432/advanced_sql_usage_assignment
    5 rows affected.
    




<table>
    <thead>
        <tr>
            <th>tracking_number</th>
            <th>weight</th>
            <th>location</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>ABC1</td>
            <td>117.57</td>
            <td>Madrid</td>
        </tr>
        <tr>
            <td>ABC2</td>
            <td>1.10</td>
            <td>Athens</td>
        </tr>
        <tr>
            <td>ABC3</td>
            <td>202.36</td>
            <td>Copenhagen</td>
        </tr>
        <tr>
            <td>ABC4</td>
            <td>22.73</td>
            <td>Berlin</td>
        </tr>
        <tr>
            <td>ABC5</td>
            <td>125.00</td>
            <td>Helsinki</td>
        </tr>
    </tbody>
</table>



2.2. Create a CTE named `heavy_shipments` that includes the tracking number, weight, and carrier name for all shipments with a weight greater than 200.

###### Solution: 


```sql
%%sql

WITH heavy_shipments (tracking_number, weight, carrier_name) AS (
SELECT s.tracking_number, s.weight, ca.name
FROM shipments s
INNER JOIN carriers ca ON ca.carrier_id = s.carrier_id
WHERE s.weight > 200
)
SELECT *
FROM heavy_shipments
LIMIT 5; -- limit result to not bloat notebook
```

     * postgresql://postgres:***@localhost:5432/advanced_sql_usage_assignment
    5 rows affected.
    




<table>
    <thead>
        <tr>
            <th>tracking_number</th>
            <th>weight</th>
            <th>carrier_name</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>ABC3</td>
            <td>202.36</td>
            <td>Carrier W</td>
        </tr>
        <tr>
            <td>ABC15</td>
            <td>200.64</td>
            <td>Carrier Y</td>
        </tr>
        <tr>
            <td>ABC48</td>
            <td>204.76</td>
            <td>Carrier U</td>
        </tr>
        <tr>
            <td>ABC105</td>
            <td>200.73</td>
            <td>Carrier Q</td>
        </tr>
        <tr>
            <td>ABC121</td>
            <td>204.18</td>
            <td>Carrier Y</td>
        </tr>
    </tbody>
</table>



#### 3. Transactions

3.1. Write a transaction that updates the status of the shipment with tracking number 'ABC123' to 'Shipped' and increments the weight by 10 units.

###### Solution: 


```sql
%%sql

BEGIN;
UPDATE Shipments 
SET status = 'Shipped',
	weight = weight + 10
WHERE tracking_number = 'ABC123';
COMMIT; 

-- verify transaction result
SELECT * 
FROM Shipments
WHERE tracking_number = 'ABC123'
```

     * postgresql://postgres:***@localhost:5432/advanced_sql_usage_assignment
    Done.
    1 rows affected.
    Done.
    1 rows affected.
    




<table>
    <thead>
        <tr>
            <th>shipment_id</th>
            <th>tracking_number</th>
            <th>weight</th>
            <th>status</th>
            <th>warehouse_id</th>
            <th>carrier_id</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>123</td>
            <td>ABC123</td>
            <td>137.85</td>
            <td>Shipped</td>
            <td>8</td>
            <td>7</td>
        </tr>
    </tbody>
</table>



3.2. Write another transaction that inserts a new shipment with tracking number 'LMN456', weight 180.75, into Warehouse B using Carrier Y.

###### Solution: 


```sql
%%sql

BEGIN;
INSERT INTO Shipments (tracking_number, weight, warehouse_id, carrier_id)
VALUES ('LMN456', 180.75, 2,2);
COMMIT; 

-- verify transaction result
SELECT * 
FROM Shipments
WHERE tracking_number = 'LMN456'
```

     * postgresql://postgres:***@localhost:5432/advanced_sql_usage_assignment
    Done.
    1 rows affected.
    Done.
    1 rows affected.
    




<table>
    <thead>
        <tr>
            <th>shipment_id</th>
            <th>tracking_number</th>
            <th>weight</th>
            <th>status</th>
            <th>warehouse_id</th>
            <th>carrier_id</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>100001</td>
            <td>LMN456</td>
            <td>180.75</td>
            <td>Pending</td>
            <td>2</td>
            <td>2</td>
        </tr>
    </tbody>
</table>



#### 4. Indexes

Consider a frequently executed query to retrieve shipments based on their tracking number.
```sql
SELECT tracking_number, weight, status
FROM shipments
WHERE tracking_number = 'XYZ789';
```

This query is essential for tracking specific shipments. However, as the dataset grows, there might be performance concerns. Enhance the performance of this query by creating an appropriate index.

###### Solution: 


```sql
%%sql

CREATE INDEX inx_tracking_number ON shipments (tracking_number);

-- verify index usage
EXPLAIN ANALYSE 
SELECT * 
FROM Shipments 
WHERE tracking_number = 'XYZ789';
```

     * postgresql://postgres:***@localhost:5432/advanced_sql_usage_assignment
    Done.
    4 rows affected.
    




<table>
    <thead>
        <tr>
            <th>QUERY PLAN</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Index Scan using inx_tracking_number on shipments  (cost=0.42..8.44 rows=1 width=144) (actual time=0.043..0.043 rows=0 loops=1)</td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;Index Cond: ((tracking_number)::text = &#x27;XYZ789&#x27;::text)</td>
        </tr>
        <tr>
            <td>Planning Time: 3.482 ms</td>
        </tr>
        <tr>
            <td>Execution Time: 0.058 ms</td>
        </tr>
    </tbody>
</table>


